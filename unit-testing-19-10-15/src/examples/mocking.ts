import { RandomProvider } from './mock-dependencies';

export class Sitcom {
  constructor(private randomProvider: RandomProvider) {}

  public getJoke(): number {
    return this.randomProvider.getRandom();
  }
}
