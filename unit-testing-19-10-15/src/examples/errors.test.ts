class TestError extends Error {
  specialStuff?: string;

  constructor(message?: string, specialStuff = '') {
    super(`[TestError] ${message}`);
    this.specialStuff = specialStuff;
  }
}

function throwsError() {
  throw new TestError('this is a test error');
}

describe('Exception handling', () => {
  test('should check if wrapped function throws error', () => {
    const functionWrapper = () => throwsError();

    expect(functionWrapper).toThrow('[TestError]');
    expect(functionWrapper).toThrow(TestError);
  });

  test('should throw exception and assert property', () => {
    const myError = new TestError('error with speciality', 'alma');

    try {
      throw myError;
      fail('should have thrown Error');
    } catch (err) {
      expect(err).toBeInstanceOf(TestError);
      expect(err).toHaveProperty('specialStuff', 'alma');
    }
  });

  test('should not throw any exception', done => {
    try {
      // throwsError(); // ???

      done();
    } catch (err) {
      fail('should not throw Error');
    }
  });
});
