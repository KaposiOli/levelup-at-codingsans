const THE_ANSWER_TO_THE_QUESTION = 42;

export interface RandomProvider {
  getRandom(): number;
}

export class RandomProviderImplementation implements RandomProvider {
  getRandom() {
    return Math.random();
  }
}
