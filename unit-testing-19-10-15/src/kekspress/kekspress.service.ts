export interface KekspressPassenger {
  name: string;
  getOffAt: number;
}

export class PassengerProvider {
  private passengers: KekspressPassenger[];
}

export class Keksception extends Error {}

export function getPassengerNames(passengers: KekspressPassenger[]): string[] {
  return passengers.map(({ name }) => name);
}

export function getNextStop(currentStop: number, skipping = 0) {
  return currentStop + skipping + 1;
}

export function filterStop(passengers: KekspressPassenger[], currentStop: number): KekspressPassenger[] {
  return passengers.filter(({ getOffAt }) => getOffAt !== currentStop);
}

export function findName(passengers: KekspressPassenger[], filterName: string) {
  return passengers.find(({ name }) => name === filterName);
}

export function filterName(passengers: KekspressPassenger[], filterName: string): KekspressPassenger[] {
  return passengers.filter(({ name }) => name !== filterName);
}

export function insertPassenger(
  passenger: KekspressPassenger,
  passengers: KekspressPassenger[],
  maxSeats: number,
): KekspressPassenger[] {
  return [...passengers, ...(passengers.length < maxSeats ? [passenger] : [])];
}
