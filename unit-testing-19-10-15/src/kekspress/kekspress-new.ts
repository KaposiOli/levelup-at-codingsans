import {
  getNextStop,
  filterStop,
  filterName,
  findName,
  getPassengerNames,
  insertPassenger,
  KekspressPassenger,
  Keksception,
} from './kekspress.service';

export class Kekspress {
  private passengers: KekspressPassenger[] = [];
  private currentStop: number = 0;

  constructor(private maxSeats: number) {}

  getPassengers(): string[] {
    return getPassengerNames(this.passengers);
  }

  nextStop(skipping = 0) {
    this.currentStop = getNextStop(this.currentStop, skipping);
    this.passengers = filterStop(this.passengers, this.currentStop);
  }

  board(name: string, getOffAt: number) {
    if (findName(this.passengers, name)) {
      throw new Keksception(`Name ${name} already boarded`);
    }
    this.passengers = insertPassenger({ name, getOffAt }, this.passengers, this.maxSeats);
  }

  getOff(name: string): KekspressPassenger | undefined {
    const passenger = findName(this.passengers, name);
    this.passengers = filterName(this.passengers, name);
    return passenger;
  }
}
