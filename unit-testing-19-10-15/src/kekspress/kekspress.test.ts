import { Kekspress } from './kekspress';
import { Keksception } from './kekspress.service';

describe('Kekspress', () => {
  describe('getPassengers', () => {
    test('should return 0 passengers if the Kekspress is empty', () => {
      const kekspress = new Kekspress(9);
      expect(kekspress.getPassengers()).toHaveLength(0);
    });

    test('should return 1 passenger if the Kekspress only passanger1 boarded the train', () => {
      // create a new passanger instance
      const kekspress = new Kekspress(9);

      // board the train with passanger1
      kekspress.board('passanger1', 3);

      // get passangers
      const passangers = kekspress.getPassengers();

      // expect that kekspress has only one passanger
      expect(passangers).toHaveLength(1);
    });
  });

  describe('nextStop', () => {
    test('should remove passangers who got off', () => {
      // create new kekspress instance
      const kekspress = new Kekspress(9);

      // board 2 passangers, passanger1 gets off at the second stop and passanger2 gets off at the third stop
      kekspress.board('passanger1', 2);
      kekspress.board('passanger2', 3);

      // skip 1 stop and move to the next (we are at the 2nd stop), passanger1 gets off
      kekspress.nextStop(1);

      // get passangers
      const passangers = kekspress.getPassengers();

      // expect that we have one passanger in the train
      expect(passangers).toHaveLength(1);
    });

    test('should use the default skipping value and remove passangers', () => {
      // create new kekspress instance
      const kekspress = new Kekspress(9);

      // board 2 passangers, passanger1 gets off at the first stop and passanger2 gets off at the third stop
      kekspress.board('passanger1', 1);
      kekspress.board('passanger2', 3);

      // move to the next stop (we are at the 1st stop), passanger1 gets off
      kekspress.nextStop();

      // get passangers
      const passangers = kekspress.getPassengers();

      // expect that we have one passanger in the train
      expect(passangers).toHaveLength(1);
    });

    test('should not remove any of the passangers if kekspress skips their stop', () => {
      // create new kekspress instance
      const kekspress = new Kekspress(9);

      // board 2 passanger, passanger1 gets off at the second stop and passanger2 gets off at the second stop
      kekspress.board('passanger1', 2);
      kekspress.board('passanger2', 2);

      // skip 2 stops and move to the next (we are at the 3rd stop)
      kekspress.nextStop(2);

      // get passangers
      const passangers = kekspress.getPassengers();

      // expect that we have two passanger on the train
      expect(passangers).toHaveLength(2);
    });
  });

  describe('board', () => {
    test('should add new passanger', () => {
      // create a new kekspress instance
      const kekspress = new Kekspress(10);

      // add passanger1
      kekspress.board('passanger1', 2);

      //get passangers
      const passangers = kekspress.getPassengers();

      //expect that kekspress has one passanger
      expect(passangers).toHaveLength(1);
    });

    test('should not add new passanger of the maximum number is reached', () => {
      // create a new kekspress instance
      const kekspress = new Kekspress(2);

      // board passanger1
      kekspress.board('passanger1', 2);

      // board passanger2
      kekspress.board('passanger2', 2);

      // board passanger3 which should be not added to the passangers
      kekspress.board('passanger3', 2);

      //get passangers
      const passangers = kekspress.getPassengers();

      //expect that kekspress has two passanger
      expect(passangers).toHaveLength(2);
    });

    test('should throw error if two passangers have the same name', () => {
      // create a new kekspress instance
      const kekspress = new Kekspress(10);

      const passangerName = 'passanger1';

      // add passanger1
      kekspress.board(passangerName, 2);

      try {
        // add passanger1 again
        kekspress.board(passangerName, 2);
      } catch (err) {
        expect(err).toEqual(new Keksception(`Name ${passangerName} already boarded`));
      }
    });
  });

  describe('getOff', () => {
    test('should remove passanger if it gets off', () => {
      // create a new kekspress instance
      const kekspress = new Kekspress(10);

      // add passanger1
      kekspress.board('passanger1', 2);

      // add passanger1
      kekspress.board('passanger2', 3);

      // get off passanger1
      kekspress.getOff('passanger1');

      //get passangers
      const passangers = kekspress.getPassengers();

      //expect that kekspress has one passanger
      expect(passangers).toHaveLength(1);
    });

    test('should not remove passanger', () => {
      // create a new kekspress instance
      const kekspress = new Kekspress(10);

      // add passanger1
      kekspress.board('passanger1', 2);

      // get off passanger1
      kekspress.getOff('passanger3');

      //get passangers
      const passangers = kekspress.getPassengers();

      //expect that kekspress has one passanger
      expect(passangers).toHaveLength(1);
    });
  });
});
