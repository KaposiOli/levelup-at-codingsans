import { janosVitez } from './janosVitez';

/*
  Modified by: Karoly Szekeyl
  Author: Burom Barna
  Date: 2019 09 29
  Location: Budapest
  Purpose: Clean Code HW
*/

function main() {
  const poem = `"No, hogy még szebb legyen," felelt a kapitány,"  Lássunk, embereim, az áldomás után;  Papok pincéjéből van jó borunk elég,  Nézzük meg a kancsók mélységes fenekét!"`;

  janosVitez(poem);
}

main();
