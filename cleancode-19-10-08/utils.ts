export function breakPoemLines(poem: string) {
  return poem.replace(new RegExp('\\s{2}', 'g'), '\n');
}

export function printLn(line: string): void {
  console.log(line, '\n');
}

export function randomInt(base: number) {
  return Math.floor(Math.random() * base);
}
