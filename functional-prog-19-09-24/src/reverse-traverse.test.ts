import { Item, ItemReverse, reverseTraverse } from './reverse-traverse';

type TestCase = {
  name: string;
  input: Item[];
  output: ItemReverse[];
};
const testCases: TestCase[] = [
  {
    name: 'should work on a single node',
    input: [{ id: '0' }],
    output: [
      {
        id: '0',
        children: [],
      },
    ],
  },
  {
    name: 'should work on disjunct nodes',
    input: [{ id: '0' }, { id: '1' }, { id: '2' }],
    output: [
      {
        id: '0',
        children: [],
      },
      {
        id: '1',
        children: [],
      },
      {
        id: '2',
        children: [],
      },
    ],
  },
  {
    name: 'should work on two disjunct trees',
    input: [{ id: '0', parent: { id: '2' } }, { id: '3', parent: { id: '4', parent: { id: '5' } } }],
    output: [
      {
        id: '2',
        children: [{ id: '0', children: [] }],
      },
      {
        id: '5',
        children: [
          {
            id: '4',
            children: [{ id: '3', children: [] }],
          },
        ],
      },
    ],
  },
  {
    name: 'should work on an empty array',
    input: [],
    output: [],
  },
  {
    name: 'should work on a 3 deep structure',
    input: [{ id: '0', parent: { id: '3' } }, { id: '2', parent: { id: '1', parent: { id: '3' } } }],
    output: [
      {
        id: '3',
        children: [
          {
            id: '0',
            children: [],
          },
          {
            id: '1',
            children: [
              {
                id: '2',
                children: [],
              },
            ],
          },
        ],
      },
    ],
  },
];

testCases.forEach(testCase =>
  test(testCase.name, () => {
    expect(reverseTraverse(testCase.input)).toStrictEqual(testCase.output);
  }),
);
